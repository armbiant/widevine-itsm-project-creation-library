# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

# 0.3.0
## Added
- Add unit test environment
## Fixed
- `create_carroyage()` function use with bbox
- Fix in feed merging process
- Fix the result scale in `calculate_density` when a `area_field` is specified
## Changed
- `create_carroyage()` function now returns a GeoDataFrame instead of writing a file to enable additional processing
- `gtfs_kit` library over `gtfstk` library

# 0.2.0
## Added
- Functions to customize GIS LinesAndDots layers
## Changed
- `gis_processing` and `feed_processing` have now new `gisp` and `feedp` aliases
- A lot of rename and API incompatibility changes
## Fixed

# 0.1.0
Initialize library
