from itsim_project_creation_library import gis_processing as gisp


def test_get_bbox():
    min_lat = 48.810800
    max_lat = 48.906071
    min_lon = 2.242104
    max_lon = 2.430588
    expected_result = [
        [min_lon, min_lat],
        [max_lon, min_lat],
        [max_lon, max_lat],
        [min_lon, max_lat],
        [min_lon, min_lat],
    ]
    assert gisp.get_bbox(
        min_lat=min_lat,
        max_lat=max_lat,
        min_lon=min_lon,
        max_lon=max_lon,
    ) == expected_result


def test_get_coordinates_from_bbox():
    min_lat = 48.810800
    max_lat = 48.906071
    min_lon = 2.242104
    max_lon = 2.430588
    bbox = [
        [min_lon, min_lat],
        [max_lon, min_lat],
        [max_lon, max_lat],
        [min_lon, max_lat],
        [min_lon, min_lat],
    ]
    assert gisp.get_coordinates_from_bbox(bbox=bbox) == (min_lat, max_lat, min_lon, max_lon)
